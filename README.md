# ExamenNetCore


## Name
Examen programador ASP NET Core C#

## Description
Este es un proyecto en el que se pueden insertar, visualizar y modificar publicaciones realizadas como si fuera un blog.

## Installation
Para poder hacer uso del proyecto se deben seguir las siguientes instrucciones:

1.- Se debe ejecutar el script de base de datos llamado **BaseDatos.sql**, ubicado en la raíz del proyecto.

2.- Abrir la solución del proyecto con **VisualStudio 2022**
3.- Abrir el archivo **appsettings.json** y modificar el valor de la variable **CadenaSql** utilizando los parámetros de su servidor **SqlServer**
     - data source: El nombre que utiliza su servidor
     - initial catalog: El nombre que se utilizó para la base de datos (en caso de haberlo modificado)
     - user id: El nombre de usuario que utiliza para conectarse al servidor
     - password: La contraseña del usuario
     - NOTA: En caso de no usar usuario y contraseña, reemplazar ambos parámetros por el siguiente integrated security=True;
4.- Abrir el archivo **appsettings.json** y modificar el valor la variable **BaseUrl** por la URL local que tiene su servidor, se tiene configurada por default, pero en caso de modificarla.
5.- Compilar y ejecutar el proyecto.

## Usage
El botón Inicio, manda al listado General de publicaciones realizadas.
Dentro, se ven las publicaciones hechas, al dar clic en una se visualizan y pueden editar los datos.
Al ir a Nueva Publicación, se pueden agregar publicaciones llenando todos los datos.
Se pueden buscar publicaciones por coincidencias de texto dentro del input buscar, también se puede filtrar la búsqueda por autor, contenido o título
El botón Recursos contiene los recursos que se utlizaron para el proyecto así como el manual de insalación del mismo.
El botón Currículum, descarga mi curriculum para su visualización.

## Authors and acknowledgment
Braulio Arambula Martinez

## Project status
Finalizado
