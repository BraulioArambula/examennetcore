CREATE DATABASE blog
GO

USE blog
GO

CREATE TABLE [dbo].[catEstatus](
	[id] [int] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_catEstatus] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[publicaciones]    Script Date: 21/11/2023 03:26:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[publicaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [varchar](100) NOT NULL,
	[autor] [varchar](50) NOT NULL,
	[fechaPublicacion] [datetime] NOT NULL,
	[contenido] [varchar](max) NOT NULL,
	[idEstatus] [int] NOT NULL,
 CONSTRAINT [PK_publicaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[publicaciones]  WITH CHECK ADD  CONSTRAINT [fk_publicaciones_idEstatus] FOREIGN KEY([idEstatus])
REFERENCES [dbo].[catEstatus] ([id])
GO
ALTER TABLE [dbo].[publicaciones] CHECK CONSTRAINT [fk_publicaciones_idEstatus]
GO
/****** Object:  StoredProcedure [dbo].[sp_editar_publicacion]    Script Date: 21/11/2023 03:26:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_editar_publicacion](
@idPublicacion int,
@titulo varchar(100) null,
@autor varchar(50) null,
@contenido varchar(MAX) null
)as
begin
	update
		publicaciones
	set
		titulo = isnull(@titulo, titulo),
		autor = isnull(@autor, autor),
		contenido = isnull(@contenido,contenido)
	where
		id = @idPublicacion
end
GO
/****** Object:  StoredProcedure [dbo].[sp_eliminar_publicacion]    Script Date: 21/11/2023 03:26:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_eliminar_publicacion](
@idPublicacion int
)as
begin
	update
		publicaciones
	set
		idEstatus = 2
	where
		id = @idPublicacion
end
GO
/****** Object:  StoredProcedure [dbo].[sp_guardar_publicacion]    Script Date: 21/11/2023 03:26:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_guardar_publicacion](
@titulo varchar(100),
@autor varchar(50),
@fechaPublicacion datetime,
@contenido varchar(MAX)
)as
begin
	insert into publicaciones(titulo, autor, fechaPublicacion, contenido,idEstatus)
	values (@titulo, @autor, @fechaPublicacion,@contenido, 1)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_lista_publicaciones]    Script Date: 21/11/2023 03:26:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_lista_publicaciones]
as
begin
	
	select
		p.id,
		p.titulo,
		p.autor,
		p.contenido,
		p.fechaPublicacion,
		e.descripcion
	from
		publicaciones p
	inner join catEstatus e 
	on p.idEstatus = e.id
	where
		p.idEstatus = 1
end
GO

