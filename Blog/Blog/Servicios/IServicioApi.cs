﻿using Blog.Models;

namespace Blog.Servicios
{
    public interface IServicioApi
    {
        Task<List<Publicacion>> Lista();
        Task<Publicacion> Obtener(int idPublicacion);
        Task<bool> Guardar(Publicacion objeto);
        Task<bool> Editar(Publicacion objeto);
        Task<bool> Eliminar(Publicacion objeto);
    }
}
