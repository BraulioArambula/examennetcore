﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using Blog.Models;
using System;

namespace Blog.Servicios
{
    public class ServicioApi : IServicioApi
    {
        private static string _User;
        private static string _Pass;
        private static string _BaseUrl;
        private static string _Token;

        public ServicioApi()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            _User = builder.GetSection("settings:User").Value.ToString();
            _Pass = builder.GetSection("settings:Pass").Value.ToString();
            _BaseUrl = builder.GetSection("settings:BaseUrl").Value.ToString();
        }

        public async Task Autenticar()
        {
            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            var credenciales = new Usuario() { user = _User, pass = _Pass };
            var content = new StringContent(JsonConvert.SerializeObject(credenciales), Encoding.UTF8, "application/json");
            var response = await cliente.PostAsync("api/Authentication/Autenticar", content);
            var jsonRespuesta = await response.Content.ReadAsStringAsync();

            var resultado = JsonConvert.DeserializeObject<ResultadoUsuario>(jsonRespuesta);
            _Token = resultado.token;
        }

        public async Task<List<Publicacion>> Lista()
        {
            List<Publicacion> lista = new List<Publicacion>();

            await Autenticar();

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _Token);
            var response = await cliente.GetAsync("api/Publicacion/Publicaciones");

            if (response.IsSuccessStatusCode)
            {
                var jsonRespuesta = await response.Content.ReadAsStringAsync();
                var resultado = JsonConvert.DeserializeObject<ResultadoApi>(jsonRespuesta);
                lista = resultado.response;
            }

            return lista;
        }

        public async Task<Publicacion> Obtener(int idPublicacion)
        {
            Publicacion objeto = new Publicacion();

            await Autenticar();

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _Token);
            var response = await cliente.GetAsync($"api/Publicacion/Obtener/{idPublicacion}");

            if (response.IsSuccessStatusCode)
            {
                var jsonRespuesta = await response.Content.ReadAsStringAsync();
                var resultado = JsonConvert.DeserializeObject<ResultadoApi>(jsonRespuesta);
                objeto = resultado.objeto;
            }

            return objeto;
        }

        public async Task<bool> Guardar(Publicacion objeto)
        {
            bool respuesta = false;

            await Autenticar();

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _Token);

            var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");
            var response = await cliente.PostAsync("api/Publicacion/Guardar/",content);

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }

        public async Task<bool> Editar(Publicacion objeto)
        {
            bool respuesta = false;

            await Autenticar();

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _Token);

            var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");
            var response = await cliente.PutAsync("api/Publicacion/Editar/", content);

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }

        public async Task<bool> Eliminar(Publicacion objeto)
        {
            bool respuesta = false;

            await Autenticar();

            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_BaseUrl);
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _Token);

            var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");
            var response = await cliente.PutAsync("api/Publicacion/Eliminar/", content);

            if (response.IsSuccessStatusCode)
            {
                respuesta = true;
            }

            return respuesta;
        }
    }
}
