﻿using Blog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Blog.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PublicacionController : ControllerBase
    {
        private readonly string cadenaSql;

        public PublicacionController(IConfiguration conf)
        {
            cadenaSql = conf.GetConnectionString("CadenaSql");
        }

        [HttpGet]
        [Route("Publicaciones")]
        public IActionResult Lista()
        {
            List<Publicacion> lista = new List<Publicacion>();
            string contenido = "";

            try
            {
                using (var conexion = new SqlConnection(cadenaSql))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("sp_lista_publicaciones", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            contenido = rd["contenido"].ToString();

                            if (contenido.Length > 70)
                                contenido = contenido.Substring(0, 70) + "...";

                            lista.Add(new Publicacion()
                            {
                                idPublicacion = Convert.ToInt32(rd["id"]),
                                titulo = rd["titulo"].ToString(),
                                autor = rd["autor"].ToString(),
                                contenido = contenido,
                                fechaPublicacion = Convert.ToDateTime(rd["fechaPublicacion"])
                            });
                        }
                    }

                    conexion.Close();
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "OK", response = lista });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = ex.Message, response = lista });
            }
        }

        [HttpGet]
        [Route("Obtener/{id:int}")]
        public IActionResult Obtener(int id)
        {
            List<Publicacion> lista = new List<Publicacion>();
            Publicacion publicacion = new Publicacion();

            try
            {
                using (var conexion = new SqlConnection(cadenaSql))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("sp_lista_publicaciones", conexion);
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (var rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            lista.Add(new Publicacion()
                            {
                                idPublicacion = Convert.ToInt32(rd["id"]),
                                titulo = rd["titulo"].ToString(),
                                autor = rd["autor"].ToString(),
                                contenido = rd["contenido"].ToString(),
                                fechaPublicacion = Convert.ToDateTime(rd["fechaPublicacion"])
                            });
                        }
                    }
                    conexion.Close();
                }

                publicacion = lista.Where(item => item.idPublicacion == id).FirstOrDefault();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "OK", objeto = publicacion });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = ex.Message, response = lista });
            }
        }

        [HttpPost]
        [Route("Guardar")]
        public IActionResult Guardar([FromBody] Publicacion obj)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSql))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("sp_guardar_publicacion", conexion);
                    cmd.Parameters.AddWithValue("titulo", obj.titulo);
                    cmd.Parameters.AddWithValue("autor", obj.autor);
                    cmd.Parameters.AddWithValue("fechaPublicacion", DateTime.Now);
                    cmd.Parameters.AddWithValue("contenido", obj.contenido);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    conexion.Close();
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "OK" });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = ex.Message });
            }
        }

        [HttpPut]
        [Route("Editar")]
        public IActionResult Editar([FromBody] Publicacion obj)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSql))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("sp_editar_publicacion", conexion);
                    cmd.Parameters.AddWithValue("idPublicacion", obj.idPublicacion == 0 ? DBNull.Value : obj.idPublicacion);
                    cmd.Parameters.AddWithValue("titulo", obj.titulo is null ? DBNull.Value : obj.titulo);
                    cmd.Parameters.AddWithValue("autor", obj.autor is null ? DBNull.Value : obj.autor);
                    cmd.Parameters.AddWithValue("contenido", obj.contenido is null ? DBNull.Value : obj.contenido);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    conexion.Close();
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "EDITADA" });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = ex.Message });
            }
        }

        //Se hace PUT por eliminación lógica
        [HttpPut]
        [Route("Eliminar")]
        public IActionResult Eliminar([FromBody] Publicacion obj)
        {
            try
            {
                using (var conexion = new SqlConnection(cadenaSql))
                {
                    conexion.Open();
                    var cmd = new SqlCommand("sp_eliminar_publicacion", conexion);
                    cmd.Parameters.AddWithValue("idPublicacion", obj.idPublicacion == 0 ? DBNull.Value : obj.idPublicacion);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    conexion.Close();
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "ELIMINADA" });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { mensaje = ex.Message });
            }
        }
    }
}
