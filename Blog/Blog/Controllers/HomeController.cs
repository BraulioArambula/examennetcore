﻿using Blog.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Blog.Servicios;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IServicioApi _ServicioApi;

        public HomeController(IServicioApi servicioApi)
        {
            _ServicioApi = servicioApi;
        }

        public async Task<IActionResult> Index(string busqueda, string buscarPor)
        {
            List<Publicacion> lista = await _ServicioApi.Lista();
            ViewBag.Busqueda = "";
            ViewBag.BuscarPor = "1";

            if(!String.IsNullOrEmpty(busqueda) && !String.IsNullOrEmpty(buscarPor))
            {
                ViewBag.Busqueda = busqueda;
                ViewBag.BuscarPor = buscarPor;

                if (buscarPor.Equals("1")) //Búsqueda por título
                    lista = lista.Where(b => b.titulo!.ToLower().Contains(busqueda.ToLower())).ToList();
                else if (buscarPor.Equals("2")) //Búsqueda por contenido
                    lista = lista.Where(b => b.contenido!.ToLower().Contains(busqueda.ToLower())).ToList();
                else if (buscarPor.Equals("3")) //Búsqueda por autor
                    lista = lista.Where(b => b.autor!.ToLower().Contains(busqueda.ToLower())).ToList();
            }

            return View(lista);
        }

        public async Task<IActionResult> Publicacion(int idPublicacion)
        {
            Publicacion objeto = new Publicacion();

            ViewBag.Accion = "Nueva Publicación";
            if (idPublicacion != 0)
            {
                objeto = await _ServicioApi.Obtener(idPublicacion);
                ViewBag.Accion = "Editar Publicación";
            }

            return View(objeto);
        }

        public ActionResult Recursos()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GuardarCambios(Publicacion objeto)
        {
            bool respuesta;

            if(objeto.idPublicacion == 0)
            {
                respuesta = await _ServicioApi.Guardar(objeto);
            }
            else
            {
                respuesta = await _ServicioApi.Editar(objeto);
            }

            if (respuesta)
                return RedirectToAction("Index");
            else
                return NoContent();
        }

        [HttpPut]
        public async Task<IActionResult> Eliminar(Publicacion objeto)
        {
            bool respuesta = await _ServicioApi.Eliminar(objeto);

            if (respuesta)
                return RedirectToAction("Index");
            else
                return NoContent();
        }
    }
}