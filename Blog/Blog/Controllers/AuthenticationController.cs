﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Blog.Models;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Diagnostics.SymbolStore;

namespace Blog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly string SecretKey;
        new private readonly string User;
        private readonly string Pass;

        public AuthenticationController(IConfiguration configuration)
        {
            SecretKey = configuration.GetSection("settings").GetSection("SecretKey").Value.ToString();
            User = configuration.GetSection("settings").GetSection("User").Value.ToString();
            Pass = configuration.GetSection("settings").GetSection("Pass").Value.ToString();
        }

        [HttpPost]
        [Route("Autenticar")]
        public IActionResult Autenticar([FromBody] Usuario request)
        {
            if (request.user.Equals(User) && request.pass.Equals(Pass))
            {
                var KeyBytes = Encoding.ASCII.GetBytes(SecretKey);
                var claims = new ClaimsIdentity();

                claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, request.user));

                var TokenDecript = new SecurityTokenDescriptor
                {
                    Subject = claims,
                    Expires = DateTime.UtcNow.AddMinutes(5),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(KeyBytes), SecurityAlgorithms.HmacSha256Signature)
                };

                var TokenHandler = new JwtSecurityTokenHandler();
                var TokenConf = TokenHandler.CreateToken(TokenDecript);

                string Token = TokenHandler.WriteToken(TokenConf);

                return StatusCode(StatusCodes.Status200OK, new { token = Token });
            }
            else
            {
                return StatusCode(StatusCodes.Status401Unauthorized, new { token = "" });
            }
        }
    }
}
