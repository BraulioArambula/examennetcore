using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Blog.Servicios;

var builder = WebApplication.CreateBuilder(args);

var ReglaCors = "ReglasCors";

builder.Services.AddCors(option =>
    option.AddPolicy(name: ReglaCors,
        builder =>
        {
            builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
        }
    )
);

// Add services to the container.

//Implementación JWT {
builder.Configuration.AddJsonFile("appsettings.json");
var SecretKey = builder.Configuration.GetSection("settings").GetSection("SecretKey").Value.ToString();
var KeyBytes = Encoding.UTF8.GetBytes(SecretKey);

builder.Services.AddAuthentication( conf =>
{
    conf.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    conf.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer( conf =>
{
    conf.RequireHttpsMetadata = false; //No requiere autenticación HTTPS
    conf.SaveToken = true; //Se guarda el Token
    conf.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true, //Validación por usuario
        IssuerSigningKey = new SymmetricSecurityKey(KeyBytes),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});
//} Implementación JWT
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<IServicioApi, ServicioApi>(); //Inyección de dependencias para que se pueda usar dentro de cualquier controlador

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication(); //JWT

app.UseCors(ReglaCors);

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
