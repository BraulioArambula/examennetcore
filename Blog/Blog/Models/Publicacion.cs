﻿namespace Blog.Models
{
    public class Publicacion
    {
        public int idPublicacion { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public DateTime fechaPublicacion { get; set; }
        public string contenido { get; set; }
        public int idEstatus { get; set; }
    }
}
