﻿namespace Blog.Models
{
    public class ResultadoApi
    {
        public string mensaje { get; set; }
        public List<Publicacion> response { get; set; }
        public Publicacion objeto { get; set; }
    }
}
